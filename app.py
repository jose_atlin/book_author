from flask import Flask, jsonify, request, g as app_ctx
from flask_cors.extension import CORS
from controller.book_controller import BookController
from configparser import ConfigParser
import time

app = Flask(__name__)
CORS(app)

# home page
@app.route('/')
def home_page():
    return "Home Page"


# USING APPLICATION CONTEXT (METHOD 1, similiar to AOP)
@app.before_request
def bef_request():
    app_ctx.start_time = time.perf_counter()


@app.after_request
def aft_request(response):
    app_ctx.total_time = time.perf_counter() - app_ctx.start_time
    print(f"Time required for request execution: {app_ctx.total_time:.5f} seconds")
    return response


# USING DECORATOR (METHOD 2, as a wrapper)
def request_wrapper(func):
    def inner_func(*args, **kwargs):
        start_t = time.perf_counter()
        response_data = func(*args, **kwargs)
        end_t = time.perf_counter()

        response_data.update({
            "elapsed_time": '%.5f' % (end_t - start_t)
        })
        return response_data

    inner_func.__name__ = func.__name__
    return inner_func


# create book
@app.route('/books', methods=['POST'])
@request_wrapper
def create_book():

    data = request.get_json()
    book_controller = BookController(data=data)
    status, book_id, author_id = book_controller.create_book()

    new_data = {
        "status": status, 
        "book_id": book_id,
        "author_id": author_id, 
    }
    data.update(new_data)
    return data


# read all books
@app.route('/books', methods=['GET'])
@request_wrapper
def read_all_books():

    book_controller = BookController()
    status, books = book_controller.read_all_book()

    data = {
        "number_of_books": len(books), 
        "status": status, 
        "books": books
    }
    return data


# read one book
@app.route('/books/<string:book_id>', methods=['GET'], endpoint='read_one_book')
@request_wrapper
def read_one_book(book_id):

    book_controller = BookController(book_id=book_id)
    status, book = book_controller.read_book_by_id()

    data = {
        "status": status, 
        "book": book
    }
    return data


# update book
@app.route('/books/<string:book_id>', methods=['PUT'], endpoint='update_book')
@request_wrapper
def update_book(book_id):

    data = request.get_json()
    book_controller = BookController(book_id=book_id, data=data)
    status = book_controller.update_book()

    new_data = {
        "status": status, 
        "book_id": book_id, 
    }
    data.update(new_data)
    return data


# delete book
@app.route('/books/<string:book_id>', methods=['DELETE'], endpoint='delete_book')
@request_wrapper
def delete_book(book_id):
    
    book_controller = BookController(book_id=book_id)
    status = book_controller.delete_book()
    
    data = {
        "status": status
    }
    return data

# start
if __name__ == "__main__":

    config_file = 'config.txt'
    config = ConfigParser()
    config.read(config_file)
    host = config.get('flask', 'host')
    port = config.get('flask', 'port')
    debug = config.get('flask', 'debug')

    app.run(host=host, port=port, debug=debug)