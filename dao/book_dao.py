from utils.connection import Database
from utils.query_manager import QueryManager

db = Database('config.txt')

class BookDAO:

    def __init__(self, *args, **kwargs):
        self.book_id = kwargs.get('book_id', None)
        self.book = kwargs.get('book', None)
        self.author = kwargs.get('author', None)
        self.query_manager = QueryManager()

    def create_book(self):

        book_query = self.query_manager.BOOK_CREATE_QUERY
        book_value = (self.book.book_name, self.book.dop, self.book.rating, 
                    self.book.pages)
        author_query = self.query_manager.AUTHOR_CREATE_QUERY
        author_value = (self.author.author_name, self.author.dob, self.author.rating)
        book_author_query = self.query_manager.BOOK_AUTHOR_REL
        book_id, author_id = None, None
        status = True

        with db.create_connection as conn:
            with db.create_cursor as cur:
                try:
                    cur.execute(book_query, book_value)
                    book_id = cur.lastrowid
                    # print("book id : ", book_id)
                except:
                    status = False
                    conn.rollback()
                try:
                    cur.execute(author_query, author_value)
                    author_id = cur.lastrowid
                    # print("author id : ", author_id)
                except:
                    status = False
                    conn.rollback()
                try:
                    book_author_value = (book_id, author_id)
                    cur.execute(book_author_query, book_author_value)
                    status = True if cur.rowcount != 0 else False
                    if status:
                        conn.commit()
                except:
                    status = False
                    conn.rollback()
        return (status, book_id, author_id)


    def read_all_book(self):

        query = self.query_manager.BOOK_READ_ALL
        books = []
        status = True

        with db.create_connection as conn:
            with db.create_cursor as cur:
                try:
                    cur.execute(query)
                    res = cur.fetchall()
                    for detail in res:
                        books.append({
                            'book_id': detail[0], 
                            'book_name': detail[1],
                            'date_of_publish': detail[2],
                            'book_rating': detail[3],
                            'pages': detail[4]
                        })
                except:
                    status = False
                    print("Mysql Error while executing found!")
        return (status, books)


    def read_book_by_id(self):

        query = self.query_manager.BOOK_READ_BY_ID
        value = (self.book_id, )
        book = {}
        status = True

        with db.create_connection as conn:
            with db.create_cursor as cur:
                try:
                    cur.execute(query, value)
                    res = cur.fetchone()
                    book['book_id'] = res[0]
                    book['book_name'] = res[1]
                    book['date_of_publish'] = res[2]
                    book['book_rating'] = res[3]
                    book['pages'] = res[4]
                except:
                    status = False
                    print("Mysql Error while executing found!")
        return (status, book)


    def update_book(self):

        query = self.query_manager.BOOK_UPDATE
        value = (self.book.book_name, self.book.dop, self.book.rating, 
            self.book.pages, self.book_id)
        status = True

        with db.create_connection as conn:
            with db.create_cursor as cur:

                try:
                    cur.execute(query, value)
                    status = True if cur.rowcount != 0 else False
                    if status:
                        conn.commit()
                except:
                    status = False
                    print("Mysql Error while executing found!")
                    conn.rollback()
        return status
        

    def delete_book(self):

        query = self.query_manager.DELETE_BOOK
        value = (self.book_id, )
        status = True

        with db.create_connection as conn:
            with db.create_cursor as cur:
                try:
                    cur.execute(query, value)
                    status = True if cur.rowcount != 0 else False
                    if status:
                        conn.commit()
                except:
                    status = False
                    print("Mysql Error while executing found!")
                    conn.rollback()
        return status