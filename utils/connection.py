import mysql.connector
from configparser import ConfigParser

class Database:
    """Database Connection Class.
    """

    def __init__(self, config_file):
        """Reads database connection parameters from the config file.

        Args:
            config_file (String): Reltive path to Config file
        """
        config = ConfigParser()
        config.read(config_file)

        self.db = config.get('database', 'db')
        self.host = config.get('database', 'host')
        self.user = config.get('database', 'user')
        self.password = config.get('database', 'password')

    @property
    def create_connection(self):
        """Creates and returns the connection specified using the db name and user details.

        Returns:
            mysql.connector.connection.MySQLConnection: connection to the db specified
        """
        self.conn = mysql.connector.connect(
            host = self.host,
            user = self.user,
            password = self.password,
            database = self.db
        )
        return self.conn;

    @property
    def create_cursor(self):
        """Creates and returns the cursor.

        Returns:
            mysql.connector.cursor.MySQLCursor: cursor for the connection
        """
        self.cur = self.conn.cursor()
        return self.cur
