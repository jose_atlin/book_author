class QueryManager:

    def __init__(self):
        pass

    @property
    def BOOK_CREATE_QUERY(self):
        return "INSERT INTO book (book_name, date_of_publish, rating, pages) VALUES (%s, %s, %s, %s)"

    @property
    def AUTHOR_CREATE_QUERY(self):
        return "INSERT INTO author (author_name, date_of_birth, rating) VALUES (%s, %s, %s)"

    @property
    def BOOK_AUTHOR_REL(self):
        return "INSERT INTO book_author_rel (book_id, author_id) VALUES (%s, %s)"

    @property
    def BOOK_READ_ALL(self):
        return "SELECT * FROM book"

    @property
    def BOOK_READ_BY_ID(self):
        return "SELECT * FROM book WHERE book_id=%s"

    @property
    def BOOK_UPDATE(self):
        return "UPDATE book SET book_name=%s, date_of_publish=%s, rating=%s, pages=%s WHERE book_id=%s"

    @property
    def DELETE_BOOK(self):
        return "call delete_book_proc(%s)"
