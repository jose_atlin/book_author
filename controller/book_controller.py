from os import stat
from dto.author_dto import Author
from dto.book_dto import Book
from service.book_servicer import BookServicer

class BookController:

    def __init__(self, *args, **kwargs):

        self.book_id = kwargs.get('book_id', None)
        self.data = kwargs.get('data', None)
        try:
            self.book_name = self.data.get('book_name', None)
            self.date_of_publish = self.data.get('date_of_publish', None)
            self.book_rating = self.data.get('book_rating', None)
            self.pages = self.data.get('pages', None)
            self.author_name = self.data.get('author_name', None)
            self.date_of_birth = self.data.get('date_of_birth', None)
            self.author_rating = self.data.get('author_rating', None)
        except AttributeError as ex:
            pass


    def check_none_type(self, *args):
        return True if all(args) is not None else False


    def check_type(self, **kwargs):
        flag = True
        try:
            self.book_id = int(kwargs.get('book_id', 0))
            self.book_rating = float(kwargs.get('book_rating', 0))
            self.pages = int(kwargs.get('pages', 0))
            self.author_rating = float(kwargs.get('author_rating', 0))
        except TypeError as ex:
            pass
        except ValueError as ex:
            flag = False
        return flag


    def create_book(self):

        status, book_id, author_id = (False, None, None)
        params_tup = (self.book_name, self.date_of_publish, self.book_rating, 
                self.pages, self.author_name, self.date_of_birth, self.author_rating)
        params_dict = {'book_rating': self.book_rating, 'pages': self.pages, 
                'author_rating': self.author_rating}

        if self.check_none_type(params_tup) and self.check_type(**params_dict):
            
            book = Book(book_name=self.book_name, dop=self.date_of_publish, rating=self.book_rating, pages=self.pages)
            author = Author(author_name=self.author_name, dob=self.date_of_birth, rating=self.author_rating)
            book_servicer = BookServicer(book=book, author=author)
            status, book_id, author_id = book_servicer.create_book()
            
        return (status, book_id, author_id)


    def read_all_book(self):

        book_servicer = BookServicer()
        status, books = book_servicer.read_all_book()
        return (status, books)


    def read_book_by_id(self):

        status, book = (False, None)
        if self.check_none_type(self.book_id) and  self.check_type(book_id=self.book_id):
            book_servicer = BookServicer(book_id=self.book_id)
            status, book = book_servicer.read_book_by_id()
        return (status, book)


    def update_book(self):

        status = False
        params_tup = (self.book_id, self.book_name, self.date_of_publish, self.book_rating, self.pages)
        params_dict = {'book_id': self.book_id, 'book_rating': self.book_rating, 'pages': self.pages}
        if self.check_none_type(params_tup) and self.check_type(**params_dict):

            book = Book(book_name=self.book_name, dop=self.date_of_publish, rating=self.book_rating, pages=self.pages)
            book_servicer = BookServicer(book_id=self.book_id, book=book)
            status = book_servicer.update_book()
        return status


    def delete_book(self):

        status = False
        if self.check_none_type(self.book_id) and  self.check_type(book_id=self.book_id):
            book_servicer = BookServicer(book_id=self.book_id)
            status = book_servicer.delete_book()
        return status