from dao.book_dao import BookDAO

class BookServicer:
    
    def __init__(self, *args, **kwargs):
        self.book_id = kwargs.get('book_id', None)
        self.book = kwargs.get('book', None)
        self.author = kwargs.get('author', None)

    def create_book(self):
        book_dao = BookDAO(book=self.book, author=self.author)
        return book_dao.create_book()

    def read_all_book(self):
        book_dao = BookDAO()
        return book_dao.read_all_book()

    def read_book_by_id(self):
        book_dao = BookDAO(book_id=self.book_id)
        return book_dao.read_book_by_id()

    def update_book(self):
        book_dao = BookDAO(book_id=self.book_id, book=self.book)
        return book_dao.update_book()

    def delete_book(self):
        book_dao = BookDAO(book_id=self.book_id)
        return book_dao.delete_book()
