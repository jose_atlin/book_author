import { create, update } from "./requests.js";

let operation = "";
let prevId = "0";

// -------------> Alert <----------------
// -------------> set alert
function setAlert(alertClass, alertType, alertMessage) {
	const template = $("#template-alert").html();
	$("#custom-alert").html(template);
	$("#alert").prop("class", alertClass);
	$("#alert-type").text(alertType);
	$("#alert-message").text(alertMessage);
}

// -------------> remove alert
$(".btn-close").on("click", function () {
	$("#custom-alert").html("");
});

// -----------> checkboxes <-------------
function checkBoxIds() {
	let highlightedBookIds = [];
	$("input:checkbox").each(function () {
		if ($(this).is(":checked")) {
			highlightedBookIds.push($(this).attr("id"));
		}
	});
	return highlightedBookIds;
}

// ------------> feature buttons <-------------
$(document).on("click", "#create, #update", function () {
	operation = this.id;
	if (operation === "update") {
		let highlightedBookIds = checkBoxIds();
		if (highlightedBookIds.length === 1) {
			$("#input-details").prop("hidden", false);
			const bookId = highlightedBookIds[0];

			const bookName = $("#book" + bookId).val();
			const dop = $("#dop" + bookId).val();
			const rating = $("#rating" + bookId).val();
			const pages = $("#pages" + bookId).val();

			$("#bookname").val(bookName);
			$("#dop").val(dop);
			$("#rating").val(rating);
			$("#pages").val(pages);
		} else if (highlightedBookIds.length > 1) {
			setAlert(
				"alert alert-danger alert-dismissible fade show",
				"Warning!!!",
				"Choose only one book to update at a time"
			);
		} else {
			setAlert(
				"alert alert-danger alert-dismissible fade show",
				"Warning!!!",
				"Select one book to update"
			);
		}
	} else {
		$("#input-details").prop("hidden", false);
	}
});

// ------------> util buttons <-----------------
// ------------> cancel
$(document).on("click", "#cancel", function () {
	$("#input-details").prop("hidden", true);
});

// -------------> trash
$(document).on("click", "#trash", function () {
	$("#bookname, #dop, #rating, #pages").val("");
});

// --------------> submit
$(document).on("click", "#submit", function () {
	let data = null;
	if (operation === "create") {
		data = JSON.stringify({
			book_name: $("#bookname").val(),
			date_of_publish: $("#dop").val(),
			book_rating: $("#rating").val(),
			pages: $("#pages").val(),
			author_name: "JAB TEST CREATE",
			date_of_birth: "JAB TEST CREATE",
			author_rating: 100.24,
		});
		create(data);
	} else if (operation === "update") {
		let highlightedBookIds = checkBoxIds();
		data = JSON.stringify({
			book_name: $("#bookname").val(),
			date_of_publish: $("#dop").val(),
			book_rating: $("#rating").val(),
			pages: $("#pages").val(),
		});
		update(data, highlightedBookIds[0]);
	}
});

// ------------> Edit <-------------
$(document).on("click", ".table-col [type=button]", editButton);

function disable(id, state) {
	const abled = $(
		"[type=text]#book" +
			id +
			", [type=text]#dop" +
			id +
			", [type=text]#rating" +
			id +
			", [type=text]#pages" +
			id
	);
	abled.prop("disabled", state);
}

function editButton() {
	const id = this.id;
	if (prevId !== id) {
		disable(id, false);
		disable(prevId, true);
		prevId = id;
	} else {
		const data = JSON.stringify({
			book_name: $("#book" + id).val(),
			date_of_publish: $("#dop" + id).val(),
			book_rating: $("#rating" + id).val(),
			pages: $("#pages" + id).val(),
		});

		disable(id, true);
		update(data, id);
		prevId = "0";
	}
}

// ---------------> page ready <---------------
// -----------> add rows
function addRow(book) {
	const template = $("#template-rows").html();
	const bookId = book.book_id;
	$("#table-row").append(template);

	$("#checkbox-id").prop("id", bookId);
	$("#book-id").prop("value", bookId);
	$("#book-id").prop("id", bookId);
	$("#name-id").prop("value", book.book_name);
	$("#name-id").prop("id", "book" + bookId);
	$("#dop-id").prop("value", book.date_of_publish);
	$("#dop-id").prop("id", "dop" + bookId);
	$("#rating-id").prop("value", book.book_rating);
	$("#rating-id").prop("id", "rating" + bookId);
	$("#pages-id").prop("value", book.pages);
	$("#pages-id").prop("id", "pages" + bookId);
	$("#edit-id").prop("id", bookId);
}

export { addRow, setAlert, checkBoxIds };
