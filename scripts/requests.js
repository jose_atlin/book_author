import { addRow, setAlert, checkBoxIds } from "./utils.js";

// -------------> CRUD OPERATIONS <-------------
// -------------> create
function create(data = null) {
	if (
		$("#bookname").val() &&
		$("#dop").val() &&
		$("#rating").val() &&
		$("#pages").val()
	) {
		$.ajax({
			url: "http://127.0.0.1:5000/books",
			type: "POST",
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			data: data,
			success: function (result) {
				console.log(result);
				if (result.status) {
					addRow(result);
					setAlert(
						"alert alert-success alert-dismissible fade show",
						"Success!!!",
						"Book is Created!"
					);
				} else {
					setAlert(
						"alert alert-danger alert-dismissible fade show",
						"Warning!!!",
						"Invalid input Parameters, please provide proper values!"
					);
				}
			},
			error: function (err) {
				console.log(err);
			},
			complete: function () {
				$("#bookname, #dop, #rating, #pages").val("");
				$("#input-details").prop("hidden", true);
			},
		});
	} else {
		setAlert(
			"alert alert-danger alert-dismissible fade show",
			"Warning!!!",
			"Please give values to all input field!"
		);
	}
}

// -----------> read all
$(function () {
	$.get("http://127.0.0.1:5000/books", function () {})
		.done(function (data) {
			for (let book of data.books) {
				addRow(book);
			}
		})
		.fail(function () {
			console.log("fail run");
		})

		.always(function () {
			console.log("always run");
		});
});

// ------------> update
function update(data = null, id) {
	$.ajax({
		url: "http://127.0.0.1:5000/books/" + id,
		type: "PUT",
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		data: data,
		success: function (result) {
			console.log(result);
			var bookId = result.book_id;
			if (result.status) {
				$("#book" + bookId).prop("value", result.book_name);
				$("#dop" + bookId).prop("value", result.date_of_publish);
				$("#rating" + bookId).prop("value", result.book_rating);
				$("#pages" + bookId).prop("value", result.pages);

				setAlert(
					"alert alert-success alert-dismissible fade show",
					"Success!!!",
					"Selected Book is updated"
				);
			} else {
				setAlert(
					"alert alert-danger alert-dismissible fade show",
					"Warning!!!",
					"Invalid input Parameters, please provide proper values!"
				);
			}
			$("#" + bookId).prop("checked", false);
		},
		error: function (err) {
			console.log(err);
		},
		complete: function () {
			$("#bookname, #dop, #rating, #pages").val("");
			$("#input-details").prop("hidden", true);
		},
	});
}

// -------------> delete
$(document).on("click", "#delete", function () {
	let highlightedBookIds = checkBoxIds();
	if (highlightedBookIds.length !== 0) {
		for (let bookId of highlightedBookIds) {
			$.ajax({
				url: "http://127.0.0.1:5000/books/" + bookId,
				type: "DELETE",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				success: function (result) {
					console.log(result);
					$("#" + bookId)
						.closest("div.table-row")
						.remove();
					setAlert(
						"alert alert-success alert-dismissible fade show",
						"Success!!!",
						"Selected Books are deleted!"
					);
				},
				error: function (err) {
					console.log(err);
				},
			});
			$("#" + bookId).prop("checked", false);
		}
	} else {
		setAlert(
			"alert alert-danger alert-dismissible fade show",
			"Warning!!!",
			"Select atleast one book to delete!"
		);
	}
});

export { create, update };
