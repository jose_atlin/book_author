class Book:

    def __init__(self, *args, **kwargs):
        self._book_name = kwargs.get('book_name', None)
        self._dop = kwargs.get('dop', None)
        self._rating = kwargs.get('rating', None)
        self._pages = kwargs.get('pages', None)

    def __repr__(self):
        return 'Author(_book_name=%s, _dop=%s, _rating=%s, _pages=%s)' % (self._book_name, self._dop, self._rating, self._pages)

    def __str__(self):
        return f"{self.author_name} was published on {self.dob} having {self.pages} pages with a rating of {self.rating}"

    @property
    def book_name(self):
        return self._book_name

    @book_name.setter
    def book_name(self, value):
        self._book_name = value

    @book_name.deleter
    def book_name(self):
        del self._book_name

    @property
    def dop(self):
        return self._dop

    @dop.setter
    def dop(self, value):
        self._dop = value

    @dop.deleter
    def dop(self):
        del self._dop

    @property
    def rating(self):
        return self._rating

    @rating.setter
    def rating(self, value):
        self._rating = value

    @rating.deleter
    def rating(self):
        del self._rating

    @property
    def pages(self):
        return self._pages

    @pages.setter
    def pages(self, value):
        self._pages = value

    @pages.deleter
    def pages(self):
        del self._pages
