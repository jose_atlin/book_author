class Author:

    def __init__(self, *args, **kwargs):
        self._author_name = kwargs.get('author_name', None)
        self._dob = kwargs.get('dob', None)
        self._rating = kwargs.get('rating', None)

    def __repr__(self):
        return 'Author(_author_name=%s, _dob=%s, _rating=%s)' % (self._author_name, self._dob, self._rating)

    def __str__(self):
        return f"{self.author_name} was born on {self.dob}, with a career rating of {self.rating}"

    @property
    def author_name(self):
        return self._author_name

    @author_name.setter
    def author_name(self, value):
        self._author_name = value

    @author_name.deleter
    def author_name(self):
        del self._author_name

    @property
    def dob(self):
        return self._dob

    @dob.setter
    def dob(self, value):
        self._dob = value

    @dob.deleter
    def dob(self):
        del self._dob

    @property
    def rating(self):
        return self._rating

    @rating.setter
    def rating(self, value):
        self._rating = value

    @rating.deleter
    def rating(self):
        del self._rating
